#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

	int limit = 60;

/////////////////////////////////stack implementation////////////////////////////////////////////////////
// A structure to represent a stack
struct Stack {
	char top;
	unsigned capacity;
	char* array;
};

// function to create a stack of given capacity. It initializes size of
// stack as 0
struct Stack* createStack(unsigned capacity)
{
	struct Stack* stack = (struct Stack*)malloc(sizeof(struct Stack));
	stack->capacity = capacity;
	stack->top = -1;
	stack->array = (int*)malloc(stack->capacity * sizeof(int));
	return stack;
}

// Stack is full when top is equal to the last index
int isFull(struct Stack* stack)
{
	return stack->top == stack->capacity - 1;
}

// Stack is empty when top is equal to -1
int isEmpty(struct Stack* stack)
{
	return stack->top == -1;
}

// Function to add an item to stack. It increases top by 1
void push(struct Stack* stack, int item)
{
	if (isFull(stack))
		return;
	stack->array[++stack->top] = item;
	printf("%d pushed to stack\n", item);
}

// Function to remove an item from stack. It decreases top by 1
int pop(struct Stack* stack)
{
	if (isEmpty(stack))
		return INT_MIN; 
	return stack->array[stack->top--];
}

// Function to return the top from stack without removing it
int top(struct Stack* stack)
{
	if (isEmpty(stack))
		return INT_MIN;
	return stack->array[stack->top];
}

void deleteStack(struct Stack* stack){
	free(stack);
}
////////////////////////////counting functions//////////////////////////////////////////////////////////////////////////

void changeBase(char n[], int currentBase, int newBase, int size1){
	//printf("%d", n[0]);
    //printf("\n");
    //printf("%d", n[1]);
    //printf("\n");
    int k;
    char entry[size1];
    for (k = 0; k<size1; k++){
      entry[k] = n[k];
      printf("%d", entry[k]-48);
      printf("\n");
    }
//    printf("%d", n[2]);
//    printf("\n");
    //printf("%d", size1);
    //printf("\n");
    double base = (double) currentBase;
    int size2 = (int) ceil(size1 * (log10(currentBase) / log10(newBase))) -1; //number of digits in the new base
    int dec = 0;
    int j;
    double i;
    for (j = 0; j<size1; j++){
      i = (double) j + 1;
      int number = entry[j];
      	//printf("%d", number - 48);
    	//printf("\n");
        switch(number){
            case 'a':
            dec += 10 * pow(base,size1-i);
          	//printf("%d", dec);
            //printf("\n");
			      break;
            case 'b':
            dec += 11 * pow(base,size1-i);
            //printf("%d", dec);
            //printf("\n");
            break;
            case 'c':
            dec += 12 * pow(base,size1-i);
            //printf("%d", dec);
            //printf("\n");
            break;
            case 'd':
            dec += 13 * pow(base,size1-i);
            //printf("%d", dec);
            //printf("\n");
            break;
            case 'e':
            dec += 14 * pow(base,size1-i);
            //printf("%d", dec);
            //printf("\n");
            break;
            case 'f':
            dec += 15 * pow(base, size1-i);
            //printf("%d", dec);
            //printf("\n");
            break;
            default:
            dec += (number - 48) * pow(base, size1-i);
            //printf("%d", n[j]-48);
            //printf("\n");
			printf("%d", dec);
            printf("\n"); 
            break;
        }
        //printf("%d", n[j]-48);
        //printf("\n");
    }
    //printf("%d", dec);
    //printf("\n");
    struct Stack *result =  createStack(size2);
    while (dec > 0){
    	int number = dec % newBase;
        switch(number){
        	case 10:
        		push(result, 97);
        		break;
            case 11:
        		push(result, 98);
        		break;
            case 12:
        		push(result, 99);
        		break;
            case 13:
        		push(result, 100);
        		break;
            case 14:
        	  push(result, 101);
        		break;
            case 15:
        		push(result, 102);
        		break;
            default:
            push(result, number + 48);
		}
        dec /= newBase;
    }
    int m; 
      for (m = 0; m<size2; m++){
      	printf("%d", top(result)-48);
      	printf("\n");
      	pop(result);
       }
    }
    
    const char* addition(char n[], char m[], int size1, int size2, int base){
    	char entry1 [size1];
    	char entry2 [size2];
    	int dif = size1 - size2;
    	int size3;
		int final = 1;
    	int i;
    	for(i = 0; i<size1; i++){
    		entry1[i] = n[i];
			// printf("%d", entry1[i]);
			// printf("\n");
    	}
    	int j;
    	for (j = 0; j<size2; j++){
    		entry2[j] = m[j];
//			printf("%d", entry2[j]);
//			printf("\n");
 		}
		// printf("%d", entry2[0]);
		// printf("\n");
		// printf("%d", entry2[1]);
		// printf("\n");
		// printf("%d", entry2[2]);
		// printf("\n");
		// printf("%d", entry2[3]);
		// printf("\n");
 		if (size1>size2){
 			size3 = size1+1;
		 }
		 else {
		 	size3 = size2+1;
		 }
		int above = 0;
		struct Stack *result = createStack(size3);
		push(result, limit);
		int a = 0;
		int k;
		for (k = size3-1; k >= 0; k--){
			int skl1, skl2;
			int number;
			if(k < size1){
			if (k >= size1) skl1 = 0;
			else {
				switch (entry1[k]){
					case 'a':
						skl1 = 10;
						//printf("%d", skl1);
						//printf("\n");
						break;
					case 'b':
						skl1 = 11;
						break;
					case 'c':
						skl1 = 12;
						break;
					case 'd':
						skl1 = 13;
						break;
					case 'e':
						skl1 = 14;
						break;
					case 'f':
						skl1 = 15;
						break;
					default:
						skl1 = (entry1[k] - 48);
						printf("skladnik1: ");
						printf("%d", skl1);
						printf("\n");
						break;
			}
	}
	if (k - dif > size2 || k-dif < 0) skl2 = 0;
	else{
				switch (entry2[k-dif]){
					case 'a':
						skl2 = 10;
						break;
					case 'b':
						skl2 = 11;
						break;
					case 'c':
						skl2 = 12;
						break;
					case 'd':
						skl2 = 13;
						break;
					case 'e':
						skl2 = 14;
						break;
					case 'f':
						skl2 = 15;
						break;
					default:
						skl2 = entry2[k-dif] - 48;
						break;
					}	
		}
		printf("skladnik2: ");
		printf("%d", skl2);
		printf("\n");
		number = skl1 + skl2 + above;
		printf("number: ");
		printf("%d", number);
		printf("\n");
		above = number / base;
		number = number % base;
		final++;
		switch(number){
			case 10:
				push(result, 97);
				break;
			case 11:
				push(result, 98);
				break;
			case 12:
				push(result, 99);
				break;
			case 13:
				push(result, 100);
				break;
			case 14:
				push(result, 101);
				break;
			case 15:
				push(result, 102);
				break;
			default:
				push(result, number + 48);
				break;
			}
		}
	}
		char *anwser = malloc(final* sizeof(char));
		anwser [0] = final;
		int z = 1;
		while (top(result) != limit){
		anwser[z] = top(result);
      	pop(result);
      	z++;
		}
//	printf(anwser);
	deleteStack(result);
	//free(anwser);
	return anwser;
}	


int main(){
char f[] = "e0000001000";
char g[] = "1a00";
int podstawa = 16;
int nowa = 10;
//changeBase(f, podstawa, nowa, 4);
char *a =  addition(f,g,11,4,podstawa);
printf("pierwsza cyfra: ");
printf("%s", a);
int b = (int) a[0];
printf("druga cyfra: ");
printf("%d", b);
free(a);
    return 0;
}
